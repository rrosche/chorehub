<table class="table table-striped table-bordered table-list">
    <thead>
        <tr>
            <th><em class="fa fa-cog"></em></th>
            <th class="hidden-xs">ID</th>
            <th>Name</th>
            <th>Current Points In Bank</th>
            <th>Total Points Earned</th>
        </tr>
    </thead>
    <tbody>

        @forelse($family->getMembers() as $member)
        <tr>
            <td align="center">
                <a class="btn btn-default"><em class="fa fa-pencil"></em></a>
                <a class="btn btn-danger"><em class="fa fa-trash"></em></a>
            </td>
            <td class="hidden-xs">{{$member->getId()}}</td>
            @if($member->getFirstName())
            <td>{{$member->getFirstName()}}&nbsp;{{$member->getLastName()}}</td>
            @else
            <td>{{$member->getEmail()}}</td>
            @endif
            <td><span class="pull-right">0</span></td>
            <td><span class="pull-right">0</span></td>
        </tr>
        @empty
        <tr>
            <td colspan="4" class="text-center">No Family Members Found</td>
        </tr>
        @endforelse
    </tbody>
</table>