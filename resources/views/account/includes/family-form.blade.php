{{ Form::open(['class' => 'form-horizontal', 'id' => 'createFamilyForm', 'data-toggle' => 'validator']) }}
<div class="">
    <h3>Create a new family</h3>
</div>
<div class="modal-title">
    Required fields indicated with <code>*</code>
</div>
<div class="modal-body">
    <div id="createFamilyAlert" class="alert alert-danger alert-block hide fade">
        <a href="#" class="close" onClick="$('#createFamilyAlert').hide()">&times;</a>
        <strong>Error!</strong>&nbsp;<span id="createFamilyErrorMessage">A problem has been occurred while submitting your data.</span>
    </div>
    <div class="well">
        <fieldset>
            <legend>Family Info</legend>

            <div class="form-group has-feedback">
                {{Form::label('familyName','Name* :',['class' => 'col-lg-2 control-label'])}}
                <div class="col-lg-10">
                    {{Form::text('familyName',null, ['class'=>'form-control has-error','placeholder'=>'Enter Family Name', 'required' => '', 'maxLength' => 50])}}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div class="modal-footer">
    <button id="saveFamilyBtn" type="submit" class="btn btn-success">
        Create Family
    </button>
</div>
{{ Form::close() }}