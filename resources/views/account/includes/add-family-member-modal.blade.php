<!-- Modal -->
<div id="addMemberModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
{{ Form::open(['url'=> 'family/add-member', 'class' => 'form-horizontal', 'id' => 'addMemberForm', 'data-toggle' => 'validator']) }}
<div class="modal-header">
    <h3>Add Points</h3>
</div>
<div class="modal-body">
    <div id="addMemberAlert" class="alert alert-danger alert-block hide fade">
        <a href="#" class="close" onClick="$('#addPointsAlert').hide()">&times;</a>
        <strong>Error!</strong>&nbsp;<span id="addMemberErrorMessage">A problem has been occurred while submitting your data.</span>
    </div>
    <div class="well">
        <fieldset>
            <legend>Family Info</legend>
            <div class="form-group has-feedback">
                {{Form::label('memberCodeInput','Member Code :',['class' => 'col-lg-2 control-label'])}}
                <div class="input-group col-lg-10">
                <span class="input-group-addon">fam_</span>
                    {{Form::text('memberCodeInput',null, ['class'=>'form-control has-error','placeholder'=>'Enter Member Code', 'required' => ''])}}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                </div>
                    <div class="help-block with-errors"></div>
            </div>
        </fieldset>
    </div>
</div>
<div class="modal-footer">
    <button id="addMemberBtn" type="submit" class="btn btn-success">
        Add Member
    </button>
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>
{{ Form::close() }}
</div>
</div>
</div>