<!-- Modal -->
<div id="familyPointsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
{{ Form::open(['class' => 'form-horizontal', 'id' => 'addPointsForm', 'data-toggle' => 'validator']) }}
<div class="modal-header">
    <h3>Add Points</h3>
</div>
<div class="modal-body">
    <div id="addPointsAlert" class="alert alert-danger alert-block hide fade">
        <a href="#" class="close" onClick="$('#addPointsAlert').hide()">&times;</a>
        <strong>Error!</strong>&nbsp;<span id="addPointsErrorMessage">A problem has been occurred while submitting your data.</span>
    </div>
    <div class="well">
        <fieldset>
            <legend>Family Info</legend>

            <div class="form-group has-feedback">
                {{Form::label('familyPointsInput','Points :',['class' => 'col-lg-2 control-label'])}}
                <div class="col-lg-10">
                    {{Form::number('familyPointsInput',null, ['class'=>'form-control has-error','placeholder'=>'Enter Family Points', 'required' => '', 'max' => (10000 - $family->getPoints()), 'min'=>'1'])}}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div class="modal-footer">
    <button id="saveFamilyBtn" type="submit" class="btn btn-success">
        Add Points
    </button>
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>
{{ Form::close() }}
</div>
</div>
</div>