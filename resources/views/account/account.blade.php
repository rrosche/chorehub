@extends('layouts.app')

@section('title','Account Settings')

@section('stylesheet-head')

@endsection

@section('javascript-head')
    <script src="{{elixir('js/util/AsyncRequest.js')}}"></script>
    <script src="{{elixir('js/account/account.js')}}"></script>
    <script>
        $(document).ready(function(){
           $('#createFamilyForm').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                e.preventDefault();
                saveFamily();
            }
           });
           $('#addPointsForm').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                e.preventDefault();
                addPoints();
            }
           });
        });
    </script>
@endsection

@section('content')
@if($errors->any())
    <div class="container">
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            {{$errors->first()}}
        </div>
    </div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Account Details
                    <div class="pull-right">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </div>
                </div>

                <div class="panel-body">
                    Email: {{Sentinel::getUser()->email}}
                    <br/> <span class="strong">Member Code: {{Sentinel::getUser()->getLookupCode()}}</span>
                    <br/> First Name: {{Sentinel::getUser()->first_name}}
                    <br/> Last Name: {{Sentinel::getUser()->last_name}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Family Details {{$family?' - '.$family->getName():''}}
                    @if($family)
                        <button class="btn btn-sm btn-primary pull-right" data-toggle="modal" data-target="#addMemberModal">Add Family Member</button>
                        <div class="clearfix"></div>
                    @endif
                </div>
    
                <div class="panel-body">
                    @if($family)
                        <h3>Remaining Points in bank:&nbsp;&nbsp;{{$family->getPoints()}}</h3>
                        <button class="btn btn-info" data-toggle="modal" data-target="#familyPointsModal">Add Points</button>
                        <hr></hr>
                        <h3>Family Members:</h3>
                        @include('account.includes.family-display')
                    @else
                        @include('account.includes.family-form')
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
{{--<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Account</div>

                <div class="panel-body">
                    <form method="post" action="/account/update">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT" /> First Name:
                        <input name="firstName" value="{{Auth::user()->firstName}}"></input>
                        <br/> Last Name:
                        <input name="lastName" value="{{Auth::user()->lastName}}"></input>
                        <br/>
                        <input type="submit" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@if($family)
    @include('account.includes.add-family-points-modal')
    @include('account.includes.add-family-member-modal')
@endif
@endsection