<!-- Modal -->
<div id="addChoreModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            {{ Form::open(['url'=> 'chores', 'class' => 'form-horizontal', 'id' => 'addMemberForm', 'data-toggle' => 'validator']) }}
            <div class="modal-header">
                <h3>Add Chore</h3>
            </div>
            <div class="modal-body">
                <div id="addMemberAlert" class="alert alert-danger alert-block hide fade">
                    <a href="#" class="close" onClick="$('#addPointsAlert').hide()">&times;</a>
                    <strong>Error!</strong>&nbsp;<span id="addMemberErrorMessage">A problem has been occurred while submitting your data.</span>
                </div>
                <div class="well">
                    <fieldset>
                        <legend>Chore Details</legend>
                        <div class="form-group has-feedback">

                            <div class="input-group col-lg-8">
                                <label for="choreName">Name:</label>
                                {{Form::text('choreName',null, ['class'=>'form-control','placeholder'=>'Chore Name', 'required' => ''])}}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>


                            <div class="input-group col-lg-8">
                                <label for="choreDescription">Description:</label>
                                {{Form::text('choreDescription',null, ['class'=>'form-control','placeholder'=>'Chore Description', 'required' => ''])}}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>

                            <div class="input-group col-lg-8">
                                <label for="choreAssignee">Assigned To:</label>
                                {{Form::select('choreAssignee',$familySelect,'S',['class'=>'form-control'])}}
                            </div>

                            <label for="dueDatePicker">Due Date:</label>
                            <div class="input-group col-lg-offset-3">
                                <div id="dueDatePicker" name="dueDatePicker"></div>
                                {{Form::hidden('choreDueDate',null, ['class'=>'form-control has-error','id'=>'choreDueDate', 'required' => ''])}}
                            </div>

                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button id="addChoreBtn" type="submit" class="btn btn-success">
                    Add Chore
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>