@extends('layouts.app')

@section('stylesheet-head')
<link type="text/css" rel="stylesheet" href="{{elixir('css/datepicker.css')}}"/>
@endsection

@section('javascript-head')
<script type="application/javascript" src="{{elixir('js/util/datepicker.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#dueDatePicker').datepicker({todayHighlight:'true'});
        $('#dueDatePicker').on("changeDate", function () {
            $('#choreDueDate').val(
                    $('#dueDatePicker').datepicker('getFormattedDate')
            );
        });
    });
</script>
@endsection

@section('content')
    @if($errors->any())
        <div class="container">
            <div class="alert alert-danger">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                {{$errors->first()}}
            </div>
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Chore Assignments (Week of {{date("m/d/y", strtotime('last monday', strtotime('next sunday')))}})</div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>{{$user->first_name?$user->first_name:$user->email}}</th>
                                    <th>Monday</th>
                                    <th>Tuesday</th>
                                    <th class="danger">Wednesday</th>
                                    <th>Thursday</th>
                                    <th>Friday</th>
                                    <th>Saturday</th>
                                    <th class="success">Sunday</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">&nbsp;Dishes</label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">&nbsp;Laundry</label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">&nbsp;Dishes</label>
                                        </div>
                                    </td>
                                    <td><div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">&nbsp;Vacuum</label>
                                        </div></td>
                                    <td>{{--empty--}}</td>
                                    <td><div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">&nbsp;Litterbox</label>
                                        </div></td>
                                    <td><div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">&nbsp;Dishes</label>
                                        </div></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Chores Created By Me
                        <button class="btn btn-sm btn-primary pull-right" data-toggle="modal" data-target="#addChoreModal">Add Chore</button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        @forelse($createdChores as $chore)
                            <div class="row">
                            <div class="col-lg-10"><font size="4"><b>{{$chore->getName()}} {{date_format($chore->getDetails()?$chore->getDetails()[0]->getDueDate(): new \DateTime,'m/d')}}</b></font>&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <div class="col-lg-5" >
                                <ul class="list-group">
                                    @foreach($chore->getDetails() as $detail)
                                        <li class="list-group-item">
                                            <a href="#">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="{{$detail->getAssignee()->getGuid()}}" checked>
                                                        @if($detail->getAssignee()->getGuid() == $user->guid)
                                                            Me
                                                        @elseif(!empty($detail->getAssignee()->getFirstName()))
                                                            {{$detail->getAssignee()->getFirstName()}}
                                                        @else
                                                            {{$detail->getAssignee()->getEmail()}}
                                                        @endif
                                                    </label>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            {{--&nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="col-lg-5 dropdown" style="display:inline-block;">
                                <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">Assign to Day(s)
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    @foreach($days as $day)
                                        <li>
                                            <a href="#">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">{{$day}}</label>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>--}}
                            </div>
                        @empty
                        {{--No Chores, Promt User to create --}}
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('chores.includes.add-chore-modal') @endsection