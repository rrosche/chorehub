@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Chore List</div>

                <div class="panel-body">
                    @foreach(Sentinel::getUser()->chores as $chore)
                        {{$chore->name}} - {{$chore->description}} <br/>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
