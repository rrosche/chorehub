<?php

use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SimpleTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testHomePage()
    {
        $this->visit('/')
            ->see('Chore Hub')
            ->dontSee("Laravel");
    }

    public function testNotLoggedIn(){
        $response = $this->call('GET', '/');
        $view = $response->original;
        $this->assertEquals('welcome', $view->getName());
    }

    public function testLoginNavigation(){
        $this->visit('/')
            ->click('Login')
            ->seePageIs('/login');
    }

    public function testLogin(){
       $this->assertTrue(true);
       // $user = factory(User::class)->create([]);
        /*$this->visit('/login')
            ->type('admin@admin.com', 'email')
            ->type('password','password')
            ->press('Login')
            ->seePageIs('/dashboard');*/
    }

    public function testRegisterNavigation(){
        $this->visit('/')
            ->click('Register')
            ->seePageIs('/register');
    }
}
