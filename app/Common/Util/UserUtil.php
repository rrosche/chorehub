<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/20/16
 * Time: 8:10 PM
 */

namespace App\Common\Util;


use App\Domain\User;
use Illuminate\Database\Eloquent\Collection;

class UserUtil {

    public static function getUsersFromCollection(Collection $userCollection){
        $users = array();

        foreach($userCollection as $userEntity){
            $users[] = self::getUserFromEntity($userEntity);
        }

        return $users;
    }

    public static function getUserFromEntity(\App\Entity\User $userEntity){
        $user = new User();
        $user->setId($userEntity->id);
        $user->setGuid($userEntity->guid);
        $user->setEmail($userEntity->email);
        $user->setFirstName($userEntity->first_name);
        $user->setLastName($userEntity->last_name);
        $user->setFamilyId($userEntity->family_id);
        $user->setPoints($userEntity->points);
        $user->setTotalPointsEarned($userEntity->total_points_earned);
        return $user;
    }
}