<?php 

namespace App\Domain;

/**
 * Class Chore
 * @package App\Domain
 */
class Chore extends Domain {

    /** @var  mixed id*/
    private $id;

    /** @var  string */
    private $name;

    /** @var  string */
    private $description;


    /** @var  User */
    private $creator;

    /** @var  ChoreDetail[] */
    private $details;

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize() {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'creator' => $this->getCreator(),
            'details' => $this->getDetails()
        );
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->description;
    }

    /**
     * @param string $name
     */
    public function setName($name){
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description){
        $this->description = $description;
    }

    /**
     * @return User
     */
    public function  getCreator(){
        return $this->creator;
    }

    /**
     * @param User $creator
     */
    public function setCreator(User $creator){
        $this->creator = $creator;
    }

    /**
     * @return ChoreDetail[]
     */
    public function getDetails(){
        return $this->details;
    }


    /**
     * @param ChoreDetail[]
     */
    public function setDetails($details){
        $this->details = $details;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }
}