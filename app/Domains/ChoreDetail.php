<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/16/16
 * Time: 10:24 PM
 */

namespace App\Domain;


/**
 * Class ChoreDetail
 * @package App\Domain
 */
class ChoreDetail extends Domain{

    /** @var  \DateTime */
    private $dueDate;

    /** @var  User */
    private $assignee;

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize() {
        return array(
            'dueDate' => $this->getDueDate()->format('d/m/Y'),
            'assignee' => $this->getAssignee(),
        );
    }

    /**
     * @return /DateTime
     */
    public function getDueDate() {
        return $this->dueDate;
    }

    /**
     * @param \DateTime $dueDate
     */
    public function setDueDate(\DateTime $dueDate) {
        $this->dueDate = $dueDate;
    }

    /**
     * @return User
     */
    public function getAssignee() {
        return $this->assignee;
    }

    /**
     * @param User $assignee
     */
    public function setAssignee($assignee) {
        $this->assignee = $assignee;
    }
}