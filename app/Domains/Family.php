<?php

namespace App\Domain;

class Family extends Domain {

    /** @var  mixed $id */
    private $id;

    /** @var  string */
    private $name;

    /** @var  int */
    private $points;

    /** @var  User[] */
    private $members;

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize() {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'points' => $this->getPoints(),
            'members' => $this->getMembers()
        );
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPoints() {
        return $this->points;
    }

    /**
     * @param int $points
     */
    public function setPoints($points) {
        $this->points = $points;
    }

    /**
     * @return User[]
     */
    public function getMembers() {
        return $this->members;
    }

    /**
     * @param User[] $members
     */
    public function setMembers($members) {
        $this->members = $members;
    }
}