<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/16/16
 * Time: 10:13 PM
 */

namespace App\Domain;


/**
 * Class User
 * @package App\Domain
 */
class User extends Domain {

    /** @var  mixed */
    private $id;

    /** @var  string */
    private $guid;

    /** @var  string */
    private $email;

    /** @var  string */
    private $firstName;

    /** @var  string */
    private $lastName;

    /** @var  mixed */
    private $familyId;

    /** @var  int */
    private $points;

    /** @var  int */
    private $totalPointsEarned;

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize() {
        return array(
            'id' => $this->getId(),
            'guid' => $this->getGuid(),
            'email' => $this->getEmail(),
            'firstName' => $this->getFirstName(),
            'lastName'  => $this->getLastName(),
            'familyId' => $this->getFamilyId(),
            'points' => $this->getPoints(),
            'totalPointsEarned' => $this->getTotalPointsEarned()
        );
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getGuid() {
        return $this->guid;
    }

    /**
     * @param string $guid
     */
    public function setGuid($guid) {
        $this->guid = $guid;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    /**
     * @return int
     */
    public function getFamilyId() {
        return $this->familyId;
    }

    /**
     * @param int $familyId
     */
    public function setFamilyId($familyId) {
        $this->familyId = $familyId;
    }

    /**
     * @return int
     */
    public function getPoints() {
        return $this->points;
    }

    /**
     * @param int $points
     */
    public function setPoints($points) {
        $this->points = $points;
    }

    /**
     * @return int
     */
    public function getTotalPointsEarned() {
        return $this->totalPointsEarned;
    }

    /**
     * @param int $totalPointsEarned
     */
    public function setTotalPointsEarned($totalPointsEarned) {
        $this->totalPointsEarned = $totalPointsEarned;
    }
}