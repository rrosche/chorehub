<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/16/16
 * Time: 10:55 PM
 */

namespace App\Repository\Implementation;

use App\Common\Util\UserUtil;
use App\Domain\Chore;
use App\Domain\ChoreDetail;
use App\Repository\ChoreRepo;
use Illuminate\Database\Eloquent\Collection;

class ChoreRepoImpl implements ChoreRepo {

    private $choreModel;
    private $userModel;


    public function __construct(\App\Entity\Chore $choreModel, \App\Entity\User $userModel) {
        $this->choreModel = $choreModel;
        $this->userModel = $userModel;
    }

    /**
     * @return \App\Domain\Chore[]
     */
    public function getAllChores() {
        $choreEntities = $this->choreModel->with('assignees','creator')->get();
        $chores = array();
        foreach ($choreEntities as $choreEntity) {
            $chores[] = $this->getChoreFromEntity($choreEntity);
        }

        return $chores;
    }

    /**
     * @param $userGuid
     * @return \App\Domain\Chore[]
     */
    public function getUserCreatedChores($userGuid) {
        $choreEntities = $this->userModel->where('guid',$userGuid)->first()->createdChores()
            ->with('assignees')->get();
        $chores = array();
        foreach($choreEntities as $choreEntity){
            $chores[] = $this->getChoreFromEntity($choreEntity);
        }
        
        return $chores;
    }

    /**
     * @param $userGuid
     * @return \App\Domain\Chore[]
     */
    public function getUserAssignedChores($userGuid) {
        $user = $this->userModel->where('guid',$userGuid)->first();
        $choreEntities = $user->chores()->with(
            ['assignees'=>function($query) use ($user) {

                $query->where('user_id','=',$user->id);

        }])->get();
          
        $chores = array();
        foreach($choreEntities as $choreEntity){
            $chores[] = $this->getChoreFromEntity($choreEntity);
        }
        
        return $chores;
    }

    /**
     * @param $choreId
     * @return \App\Domain\Chore
     */
    public function getChoreById($choreId) {
        $choreEntity = $this->choreModel
            ->find($choreId)->with('assignees')->get();
        return $this->getChoreFromEntity($choreEntity);
    }

    /**
     * @param \App\Entity\Chore $choreEntity
     * @return Chore
     */
    private function getChoreFromEntity(\App\Entity\Chore $choreEntity){
        $chore = new Chore();
        $chore->setId($choreEntity->id);
        $chore->setName($choreEntity->name);
        $chore->setDescription($choreEntity->description);
        $chore->setCreator(UserUtil::getUserFromEntity($choreEntity->creator));
        if($choreEntity->assignees){
            $chore->setDetails($this->getChoreDetailsFromEntity($choreEntity->assignees));
        }
        return $chore;
    }

    /**
     * @param Collection $details
     * @return ChoreDetail[]
     */
    private function getChoreDetailsFromEntity(Collection $details){
        $choreDetails = array();
        foreach ($details as $assignee){
            $choreDetail = new ChoreDetail();
            $choreDetail->setAssignee(UserUtil::getUserFromEntity($assignee));
            $choreDetail->setDueDate(new \DateTime($assignee->pivot->due_date));
            $choreDetails[] = $choreDetail;
        }

        return $choreDetails;
    }
}