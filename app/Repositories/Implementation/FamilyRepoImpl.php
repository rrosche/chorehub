<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/16/16
 * Time: 10:55 PM
 */

namespace App\Repository\Implementation;


use App\Domain\Family;
use App\Entity\User;
use App\Repository\FamilyRepo;
use App\Common\Util\UserUtil;

class FamilyRepoImpl implements FamilyRepo {


    private $familyModel;
    private $userModel;

    public function __construct(\App\Entity\Family $familyModel, User $userModel){
        $this->familyModel = $familyModel;
        $this->userModel = $userModel;
    }

    /**
     * @return \App\Domain\Family[]
     */
    public function getAllFamilies() {
        $familyEntities = $this->familyModel->with('members')->get();
        $families = array();
        foreach ($familyEntities as $entity){
            $families[] = $this->getFamilyFromEntity($entity);
        }

        return $families;
    }

    /**
     * @param $userGuid
     * @return \App\Domain\Family
     */
    public function getUserFamily($userGuid) {
        /** @noinspection PhpUndefinedMethodInspection */
        $familyEntity = $this->userModel->where('guid',$userGuid)->first()->family()->with('members')->get()->first();

        return $this->getFamilyFromEntity($familyEntity);

    }

    /**
     * @param $familyId
     * @return \App\Domain\Family
     */
    public function getFamilyById($familyId) {
        /** @noinspection PhpUndefinedMethodInspection */
        $familyEntity = $this->familyModel->where('id',$familyId)->with('members')->first();

        return $this->getFamilyFromEntity($familyEntity);
    }

    /**
     * @param \App\Entity\Family $familyEntity
     * @return Family
     */
    private function getFamilyFromEntity(\App\Entity\Family $familyEntity){
        $family = new Family();
        $family->setId($familyEntity->id);
        $family->setName($familyEntity->name);
        $family->setPoints($familyEntity->points);
        $family->setMembers(UserUtil::getUsersFromCollection($familyEntity->getMembers()));
        return $family;
    }
}