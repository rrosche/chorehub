<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/16/16
 * Time: 10:52 PM
 */

namespace App\Repository;

/**
 * Interface ChoreRepo
 * @package App\Repository
 */
interface ChoreRepo {
    /**
     * @return \App\Domain\Chore[]
     */
    public function getAllChores();

    /**
     * @param $userGuid
     * @return \App\Domain\Chore[]
     */
    public function getUserCreatedChores($userGuid);
    
    /**
     * @param $userGuid
     * @return \App\Domain\Chore[]
     */
    public function getUserAssignedChores($userGuid);

    /**
     * @param $choreId
     * @return \App\Domain\Chore
     */
    public function getChoreById($choreId);
}