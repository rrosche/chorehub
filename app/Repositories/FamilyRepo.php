<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/16/16
 * Time: 10:52 PM
 */

namespace App\Repository;

/**
 * Interface Family
 * @package App\Repository
 */
interface FamilyRepo {
    /**
     * @return \App\Domain\Family[]
     */
    public function getAllFamilies();

    /**
     * @param $userGuid
     * @return \App\Domain\Family
     */
    public function getUserFamily($userGuid);

    /**
     * @param $familyId
     * @return \App\Domain\Family
     */
    public function getFamilyById($familyId);
}