<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/18/16
 * Time: 5:37 AM
 */

namespace App\Providers;


use App\Entity\Chore;
use App\Entity\User;
use App\Repository\ChoreRepo;
use App\Repository\Implementation\ChoreRepoImpl;
use Illuminate\Support\ServiceProvider;

class ChoreRepoProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(ChoreRepo::class, function(){
            return new ChoreRepoImpl(new Chore(), new User());
        });
    }
}