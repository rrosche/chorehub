<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/18/16
 * Time: 6:03 AM
 */

namespace App\Providers;


use App\Repository\ChoreRepo;
use App\Service\ChoreService;
use App\Service\Implementation\ChoreServiceImpl;
use Illuminate\Support\ServiceProvider;

class ChoreServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(ChoreService::class, function($app){
           return new ChoreServiceImpl(
               $app->make(ChoreRepo::class)
           );
        });
    }
}