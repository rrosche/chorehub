<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/18/16
 * Time: 6:03 AM
 */

namespace App\Providers;


use App\Repository\FamilyRepo;
use App\Service\FamilyService;
use App\Service\Implementation\FamilyServiceImpl;
use Illuminate\Support\ServiceProvider;

class FamilyServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(FamilyService::class, function($app){
           return new FamilyServiceImpl(
               $app->make(FamilyRepo::class)
           );
        });
    }
}