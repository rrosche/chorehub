<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/18/16
 * Time: 5:37 AM
 */

namespace App\Providers;


use App\Entity\Family;
use App\Entity\User;
use App\Repository\FamilyRepo;
use App\Repository\Implementation\FamilyRepoImpl;
use Illuminate\Support\ServiceProvider;

class FamilyRepoProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(FamilyRepo::class, function(){
            return new FamilyRepoImpl(new Family(), new User());
        });
    }
}