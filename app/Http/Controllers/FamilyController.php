<?php

namespace App\Http\Controllers;

use App\Entity\User;
use Validator;
use Sentinel;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Exceptions\Traits\RestExceptionHandlerTrait;
use App\Entity\Family;
use App\Http\Requests;

class FamilyController extends Controller
{
    use RestExceptionHandlerTrait;
    
    public function __constructor(){
        $this->middleware('sentinel.auth');
    }
    

    
    public function postCreateFamily(Request $request){
         $validator = Validator::make($request->all(), [
            'familyName' => 'required|max:50'
        ]);
        if ($validator->fails()) {
           return $this->badRequest($validator->errors()->first('familyName'));
        }else{
            $family = Family::create(['name' => $request->input('familyName')]);
            Sentinel::getUser()->family()->associate($family);
            Sentinel::getUser()->save();
            return response()->json(['name' => $request->input('familyName')]);
        }
    }
    
    public function postAddPoints(Request $request){
        $family = Sentinel::getUser()->family;
        $validator = Validator::make($request->all(),[
            'familyPointsInput' => 'required|numeric|min:|max:'.(10000-$family->points)
        ]);
        
        if($validator->fails()){
            return $this->badRequest($validator->errors()->first('familyPointsInput'));
        }else{
            $family->points += $request->input('familyPointsInput');
            $family->save();
            return response()->json(['points' => $request->input('familyPointsInput')]);
        }
    }

    public function postAddMember(Request $request){
        xdebug_break();
        $memberLookupCode = $request->input('memberCodeInput');
        $user = User::where('lookup_code', '=', 'fam_'.$memberLookupCode)->first();
        if($user && $user->family_id == null){
            $family = Sentinel::getUser()->family;
            $user->family()->associate($family);
            $user->save();
            return redirect()->action('AccountController@index');
        }else{
            return redirect()->action('AccountController@index')->withErrors(array("addMemberError" => "The Member Code entered is invalid or the user already belongs to a family.  Please verify the Member Code entered is correct."));
        }
    }
}
