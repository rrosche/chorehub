<?php

namespace App\Http\Controllers;

use App\Service\FamilyService;
use Cartalyst\Sentinel\Sentinel;
use Log;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('sentinel.auth');
        
    }

    /**
     * Display a listing of the resource.
     *
     * @param FamilyService $familyService
     * @param Sentinel $sentinel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(FamilyService $familyService, Sentinel $sentinel)
    {
        $family = $familyService->getUserFamily($sentinel->getUser()->guid);
        return view('account/account')->with(
            'family',$family
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Log::info("Hello: ".$request->input('username'));
        $request->user()->firstName = $request->input('firstName');
        $request->user()->lastName = $request->input('lastName');
        $request->user()->save();
        return redirect()->action('AccountController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
