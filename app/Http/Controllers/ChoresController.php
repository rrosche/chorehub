<?php

namespace App\Http\Controllers;

use App\Entity\Chore;
use App\Entity\User;
use App\Exceptions\Traits\RestExceptionHandlerTrait;
use App\Service\ChoreService;
use App\Service\FamilyService;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Http\Request;
use Validator;

use App\Http\Requests;

class ChoresController extends Controller
{
    use RestExceptionHandlerTrait;

    public function __construct(){
        $this->middleware('sentinel.auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(ChoreService $choreService, FamilyService $familyService, Sentinel $sentinel)
    {
        /**@var \App\Entity\User*/
        $user = $sentinel->getUser();
        $userFamily = $familyService->getUserFamily($user->guid);
        $chores = $choreService->getUserAssignedChores($user->guid);
        $createdChores = $choreService->getUserCreatedChores($user->guid);
        
        $daysOfTheWeek = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ];
        
        $familySelect = array();
        foreach($userFamily->getMembers() as $member){
            $familySelect[$member->getGuid()] = $member->getEmail();
        }
        
        return view('chores.chores')->with([
            'createdChores' => $createdChores,
            'assignedChores' => $chores,
            'user' => $user,
            'family' => $userFamily,
            'days' => $daysOfTheWeek,
            'familySelect' => $familySelect
        ]);
    }

    public function postCreateChore(Request $request){
        $validator = Validator::make($request->all(), [
            'choreName' => 'required|max:50'
        ]);
        if ($validator->fails()) {
            return $this->badRequest($validator->errors()->first('familyName'));
        }else{
            return $this->badRequest("Passed");
            /*
                $family = Family::create(['name' => $request->input('familyName')]);
                Sentinel::getUser()->family()->associate($family);
                Sentinel::getUser()->save();
                return response()->json(['name' => $request->input('familyName')]);
            */
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, Sentinel $sentinel)
    {
        $validator = Validator::make($request->all(), [
            'choreName' => 'required|max:50',
            'choreDescription' => 'required|max:500',
            'choreAssignee'=>'required',
            'choreDueDate'=>'required'
        ]);

        $due_date = new \DateTime($request->input('choreDueDate'));
        $choreName = $request->input('choreName');
        $assignee = $request->input('choreAssignee');
        $choreDescription = $request->input('choreDescription');


        if ($validator->fails()) {
            return redirect()->action('ChoresController@index')->withErrors(array("addChoreError" => "Failed."));
        }else{
            //return redirect()->action('ChoresController@index')->withErrors(array("addChoreError" => "Passed. ".$due_date->format('m-d-y')." $chore->assignee"));

            $chore = new Chore([
                'name'=>$request->input('choreName'),
                'description'=>$request->input('choreDescription')
            ]);

            $chore = $sentinel->getUser()->createdChores()->save($chore);

            $assignee = User::where('guid',$assignee)->first();
            $chore->assignees()->attach($assignee, ['due_date'=>$due_date]);
            return redirect()->action('ChoresController@index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
