<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property string name
 * @property Collection assignees
 * @property string description
 */
class Chore extends Model
{
    protected $fillable = [
        'name', 'description'
    ];
    
    public function creator(){
        return $this->belongsTo(User::class);
    }
    
    public function assignees(){
        return $this->belongsToMany(User::class, 'user_chores')
                ->withPivot('due_date');
    }
    
    public function getDueDate(){
        return $this->pivot->due_date;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function getDescription(){
        return $this->description;
    }
}
