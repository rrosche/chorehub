<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Collection members
 * @property int id
 * @property string name
 * @property int points
 * @property string created_at
 * @property string updated_at
 */
class Family extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name','points'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function members() {
        return $this->hasMany(User::class, 'family_id');
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPoints(){
        return $this->points;
    }

    /**
     * @return string
     */
    public function getCreatedDate(){
        return $this->created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedDate(){
        return $this->updated_at;
    }

    /**
     * @return Collection
     */
    public function getMembers(){
        return $this->members;
    }
}
