<?php

namespace App\Entity;

use Cartalyst\Sentinel\Users\EloquentUser;

/**
 * @property int id
 * @property string guid
 * @property string email
 * @property string first_name
 * @property string last_name
 * @property int family_id
 * @property int points
 * @property int total_points_earned
 */
class User extends EloquentUser
{
    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'email',
        'guid',
        'password',
        'last_name',
        'first_name',
        'permissions',
        'total_points_earned',
        'points',
        'lookup_code'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'permissions',
        'password',
        'remember_token',
        'last_login',
        'created_at',
        'updated_at',
    ];
    
    public function chores(){
        return $this->belongsToMany(Chore::class, 'user_chores')->
                withPivot('due_date');
    }
    
    public function createdChores(){
        return $this->hasMany(Chore::class, 'creator_id');
    }
    
    public function family(){
        return $this->belongsTo(Family::class);
    }

    public function getLookupCode(){
        return $this->attributes['lookup_code'];
    }
    
    public function getFirstName(){
        return $this->first_name;
    }
    
    public function getLastName(){
        return $this->last_name;
    }
    
    public function getEmail(){
        return $this->email;
    }
}
