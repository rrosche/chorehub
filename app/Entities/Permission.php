<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/15/16
 * Time: 3:32 PM
 */

namespace App\Entity;


use Illuminate\Database\Eloquent\Model;

class Permission extends Model {
    protected $fillable = ['name', 'display_name'];
}