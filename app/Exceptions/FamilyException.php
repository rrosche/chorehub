<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/18/16
 * Time: 6:48 AM
 */

namespace App\Exceptions;


class FamilyException extends \Exception {

    public function __construct($message, \Exception $previous = null, $code = 0) {
        parent::__construct($message, $code, $previous);
    }
}