<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/18/16
 * Time: 6:44 AM
 */
namespace App\Service\Implementation;

use App\Repository\ChoreRepo;
use App\Service\ChoreService;

class ChoreServiceImpl implements ChoreService {
    
    private $choreRepo;
    
    public function __construct(ChoreRepo $choreRepo){
        $this->choreRepo = $choreRepo;   
    }
    
     /**
     * @return \App\Domain\Chore[]
     */
    public function getAllChores(){
        return $this->choreRepo->getAllChores();
    }

    /**
     * @param $userGuid
     * @return \App\Domain\Chore[]
     */
    public function getUserCreatedChores($userGuid){
        return $this->choreRepo->getUserCreatedChores($userGuid);
    }
    
    /**
     * @param $userGuid
     * @return \App\Domain\Chore[]
     */
    public function getUserAssignedChores($userGuid){
        return $this->choreRepo->getUserAssignedChores($userGuid);
    }

    /**
     * @param $choreId
     * @return \App\Domain\Chore
     */
    public function getChoreById($choreId){
        return $this->choreRepo->getChoreById($choreId);
    }
}