<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/18/16
 * Time: 6:44 AM
 */

namespace App\Service\Implementation;


use App\Exceptions\FamilyException;
use App\Repository\FamilyRepo;
use App\Service\FamilyService;

class FamilyServiceImpl implements FamilyService {

    private $familyRepo;

    public function __construct(FamilyRepo $familyRepo) {
        $this->familyRepo = $familyRepo;
    }

    /**
     * @return \App\Domain\Family[]
     * @throws FamilyException
     */
    public function getAllFamilies() {
        try{
            return $this->familyRepo->getAllFamilies();
        }catch (\Exception $e){
            throw new FamilyException("Error retrieving all families.",$e);
        }
    }

    /**
     * @param $userGuid
     * @return \App\Domain\Family
     * @throws FamilyException
     */
    public function getUserFamily($userGuid) {
        try{
            return $this->familyRepo->getUserFamily($userGuid);
        }catch (\Exception $e){
            throw new FamilyException("Error retrieving family for user: $userGuid",$e);
        }
    }

    /**
     * @param $familyId
     * @throws FamilyException
     */
    public function getFamilyById($familyId) {
        try{
            $this->familyRepo->getFamilyById($familyId);
        }catch (\Exception $e){
            throw new FamilyException("Error retrieving family: $familyId.", $e);
        }
    }
}