<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/18/16
 * Time: 6:39 AM
 */

namespace App\Service;


use App\Exceptions\FamilyException;

interface FamilyService {

    /**
     * @return \App\Domain\Family[]
     * @throws FamilyException
     */
    public function getAllFamilies();

    /**
     * @param $userGuid
     * @return \App\Domain\Family
     * @throws FamilyException
     */
    public function getUserFamily($userGuid);

    /**
     * @param $familyId
     * @throws FamilyException
     */
    public function getFamilyById($familyId);
}