<?php
/**
 * Created by PhpStorm.
 * User: rockyrosche
 * Date: 10/16/16
 * Time: 10:52 PM
 */

namespace App\Service;

/**
 * Interface ChoreService
 * @package App\Service
 */
interface ChoreService {
    /**
     * @return \App\Domain\Chore[]
     */
    public function getAllChores();

    /**
     * @param $userGuid
     * @return \App\Domain\Chore[]
     */
    public function getUserCreatedChores($userGuid);
    
    /**
     * @param $userGuid
     * @return \App\Domain\Chore[]
     */
    public function getUserAssignedChores($userGuid);

    /**
     * @param $choreId
     * @return \App\Domain\Chore
     */
    public function getChoreById($choreId);
}