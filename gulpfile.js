var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss')
        .browserify('app.js')
        .copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/', 'public/build/fonts/bootstrap')
        .styles(
            'account/account.css',
            'public/css/account/account.css'
        ).styles(
            'bootstrap-datepicker3.min.css',
            'public/css/datepicker.css'
        ).scripts(
            'bootstrap-datepicker.min.js',
            'public/js/util/datepicker.min.js'
        ).scripts(
            'util/validator.js', 
            'public/js/util/validator.js'
        ).scripts(
            'util/AsyncRequest.js', 
            'public/js/util/AsyncRequest.js'
        ).scripts(
            'account/account.js',
            'public/js/account/account.js'
        ).version([
            'css/app.css',
            'css/account/account.css',
            'css/datepicker.css',
            'js/app.js',
            'js/util/validator.js',
            'js/util/AsyncRequest.js',
            'js/account/account.js',
            'js/util/datepicker.min.js'
        ]);
});
