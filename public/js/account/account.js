 function success(result) {
     $("#createFamilyPanel").modal('hide');
 }

 function failure(result) {
     var errorMsg = result.responseJSON.error;
     $("#createFamilyErrorMessage").html(errorMsg);
     $("#createFamilyAlert").removeClass('hide');
     $("#createFamilyAlert").addClass('in');
     $("#createFamilyAlert").show();
     //alert('failure');
 }
 
 function addPointsSuccess(result){
     alert(result.responseJSON);
 }
 
 function addPointsFail(result){
     var errorMsg = result.responseJSON.error;
     $("#addPointsErrorMessage").html(errorMsg);
     var alertBox = $("#addPointsAlert").removeClass('hide');
     alertBox.removeClass('hide');
     alertBox.addClass('in');
     alertBox.show();
 }

 function successAlert(result){
     alert(result.responseJSON);
 }

 function failAlert(result){
     alert('fail');
 }

 function saveFamily() {
     var textInput = $("input[name='familyName']");
     if ($.trim(textInput.eq(0).val())) {
         var data = $("#createFamilyForm").serializeArray();
         new AsyncRequest(
             '/family/create-family',
             data,
             AsyncRequest.POST,
             success,
             failure
         ).send();
     }
 }
 
 function addPoints(){
     var data = $("#addPointsForm").serializeArray();
     new AsyncRequest(
         'family/add-points',
         data,
         AsyncRequest.POST,
         addPointsSuccess,
         addPointsFail
     ).send();
 }

 function addFamilyMember(){
     var data = $("#addMembersForm").serializeArray();
    new AsyncRequest(
        'family/add-member',
        data,
        AsyncRequest.POST,
        successAlert,
        failAlert
    ).send();
 }
//# sourceMappingURL=account.js.map
