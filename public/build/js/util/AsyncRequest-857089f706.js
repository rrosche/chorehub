var AsyncRequest = function(url,data,requestType,onSuccess, onFailure){
    this.url = url;
    this.data = data;
    this.requestType = requestType;
    this.onSuccess = onSuccess;
    this.onFailure = onFailure;
};

AsyncRequest.prototype.send = function(){
    $.ajax({
        url: this.url,
        data: this.getJSONFromFormArray(),
        dataType:'json',
        contentType: 'application/json; charset=utf-8',
        type: this.requestType,
        success: this.onSuccess,
        error: this.onFailure
    });
};

AsyncRequest.prototype.getJSONFromFormArray = function(){
    if(this.data == null ||this.data.length < 1) return data;
    var jsonArray = {};
    for(var i = 0; i < this.data.length; i++){
        var key = this.data[i].name;
        jsonArray[key] = this.data[i].value;
    }
    jsonArray = JSON.stringify(jsonArray);
    return jsonArray;
};

AsyncRequest.POST = "POST";
AsyncRequest.GET = "GET";
//# sourceMappingURL=AsyncRequest.js.map
